// ==UserScript==
// @name     Line-gradient reading help (inspired by Beeline Reader)
// @version  1.1
// @grant    none
// @run-at document-idle
// @license GPL-3.0-or-later
// ==/UserScript==

/*
Copyright Florian S. (gitlab.com/florian-s)
This program is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>.
*/

//colors to use for the gradient
//light or dark refers to the text color not the background
const COLORS_DARK = ["#1a1b41", "#ff4200", "#1a1b41", "#e50695"];
const COLORS_LIGHT = ["#ffffff", "#ff4200", "#ffffff", "#e50695"];

/** 
 *
 * Using the "Parse don't validate" methodology by Alexis King
 * CSSDim represents a CSS dimension (value + unit) and provides a static `parse` method to generate a CSSDim
 *
 **/
class CSSDim {
  
  /**
   * @param {Number} value The value of the property can be not numeric
   * @param [String} unit The unit of this CSS value
   **/
	constructor(value, unit) {
    if(isNaN(value) || !unit) throw new TypeError("Wrong types for value and unit of this CSSDIM");
    
    this.value = value;
    this.unit = unit;
  }

  /**
   * Parse a CSSOM String
   * @param {String} dimString A CSSOM String extracted from style informations
   * @return {Object} A result object containing a success key and either value or error
   *				success {boolean} is True if the parse was successful, False otherwise
   *        value {CSSDim) is set when success is true, represents the parsed CSSDim
   *        error {String} is set when success is false, gives information on the erreor
   *
   * Note : in this script, we do not check everything, we trust the value to not be complete garbage
   **/
  static parse(dimString) {
    let unit = CSSDim.getUnit(dimString);
    let parsedValue = parseFloat(dimString);
    
    if(isNaN(parsedValue) || unit === null) {
      return {success: false, error: "This CSS string is not a CSS dimension"};
    }
    
    return {success: true, value: new CSSDim(parsedValue, unit)};
  }

  /**
	 * Find the unit from a CSSOM String, for values with no dimension/unit : return null
   *
   * @param {String} dimString A CSSOM String extracter from style informations
   * @return {String|null} The unit or null
   *
   * Note : in this script we only look for the most common units
   **/
  static getUnit(dimString) {
    
    let units = dimString.match(/(em|rem|px|pt)$/)    //let's consider only the most common units
    if( units == null ) return null;
    
    return units[units.length-1];
  }
  
}

/**
 * Create the string that will become the value of the background property for a paragraph
 * We use a linear gradient background whose height is equal to the line-height and that is repeated (the number of lines)
 * We rotate colors so that each line ends with the same color the next line starts with.
 * For example : 
 *
 * By combining this background with `background-clip: text` and `color: transparent` we get an effect similar to the original
 * Ours looks slightly better because the gradient is continuous instead of per-letter.
 *
 * @param {list} colors List of colors to use
 * @param {CSSDim} lineHeight The line height of the paragraph
 * @param {Number} nbOfLines Line number in the paragraph
 * @param {CSSDim} paddingTop The top padding
 * 
 **/
function getGradientBg(colors, lineHeight, nbOfLines, paddingTop) {

  let bgString = "";
  for(i = 0; i < nbOfLines; i++) {
    
    let topPos;
    if(paddingTop.value != 0) {
    	topPos = `calc(${i*lineHeight.value}${lineHeight.unit} + ${paddingTop.value}${paddingTop.unit})`; //we use css calc to deal with different units
    } else {
      topPos = `${i*lineHeight.value}${lineHeight.unit}`;
    }
    
    bgString += `linear-gradient(90deg, ${colors[i%colors.length]}, ${colors[(i+1)%colors.length]}) top ${topPos} left/100% ${lineHeight.value}${lineHeight.unit} no-repeat`;
    if(i < nbOfLines - 1) bgString += ", ";
  }
  return bgString;
}

/**
 * Calculate the line-height using the css property or using the font-size property if the line-height is not dimensioned
 *
 * line-height can be a non-dimensioned float we have to multiply it to the font-size to get value
 * line-height is often kept as its default value `normal`, we take this into account by using fallback value
 *
 * @param {String} lineHeightProperty A CSSOM String extracted from CSS property line-height
 * @param {String} fontSizeProperty A CSSOM String extracted from sCSS property font-size
 * 
 * @return {CSSDim} The line-height as a CSSDim
 *
 */
function calculateLineHeight(lineHeightProperty, fontSizeProperty) {
  const NORMAL_RATIO = 1.2; //if line-height is normal, this is the most common ratio according to MDN
  const DEFAULT_FONT_SIZE = 16; //if we can't find a font-size, this is a common default font-size
  
  let lineHeight_parse = CSSDim.parse( lineHeightProperty );

  if( lineHeight_parse.success ) {
    return lineHeight_parse.value;
  }
  
  //Convert the value of font-size to a ratio we can use with font-size
  let ratio = lineHeightProperty.match(/^\d*\.?\d*$/) ? 
      					parseInt(lineHeightProperty) :
  							NORMAL_RATIO;
  
  let fontSize_parse = CSSDim.parse( fontSizeProperty );
  let fontSize = fontSize_parse.success ? fontSize_parse.value : new CSSDim(DEFAULT_FONT_SIZE, "px");

  return lineHeight = new CSSDim(ratio*fontSize.value, fontSize.unit); //don't miss the ratio here
}

/**
 * Determine if a color is more dark than light. The threshold is a luminosity of 50%.
 * Defaults to true if the color cannot be identified.
 *
 * @param {String} colorString A CSSOM String extracted from a CSS property that defines a color.
 * 
 * @return {boolean} True if the color is rather dark, false it is more light than dark.
 *
 */
function isColorDark(colorString) {
  let textColorRGB = colorString.match(/^rgba?\s*\(\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*(?:,\s*\d+\s*)?\)$/i); //browsers usually return the color as rgb/rgba 
  if( textColorRGB !== null ) {
    //textColorRGB wil be [full_string, r, g, b]
  	return (parseInt(textColorRGB[1]) + parseInt(textColorRGB[2]) + parseInt(textColorRGB[3]) )/3 < (255/2);
  }
  //default value
  return true;
}
/**
 * Add an explicit color to the elements if they have a background. Otherwise, as children of the <p> their text will become transparent
 * but their own background will be displayed with no clipping. It will only make the text disappear.
 * We need to repeat this for the children of these elements to take into account nested elements.
 * 
 * @param {NodeList} elements List of elements to set explicit color to
 *
 */
function setExplicitColorOnChildren(elements) {
  let length = elements.length;
  for(let i = 0; i < length; i++) {
    
    if(elements[i].children.length > 0) setExplicitColorOnChildren(elements[i].children);
    
    let style = getComputedStyle(elements[i]);
    
    //browsers usually return the color as rgb/rgba. So we are looking for values that are not rgba with alpha != with this regex
    //to detect non-transparent background
    if(!style.backgroundColor.match(/^rgba\s*\(\s*\d+\s*,\s*\d+\s*,\s*\d+\s*,\s*0+s*\)$/i)) {
      elements[i].style.color = style.color;
    } 
  }
}

/**
 * Apply the per-line gradient style to a paragraph by calculating the necessary parameters and calling getGradientBg(colors, lineHeight, nbOfLines, paddingTop).
 * Uses CSSDim to parse the CSS styles as needed.
 *
 * @param {DomElement} p The paragraph that will be restyled
 * 
 **/
function lineGradientThisP(p) {
  
  let rect = p.getBoundingClientRect();
  if (rect.height == 0) return; //often means the <p> is not visible : skip it to avoid making it transparent with no bg
 
  let pStyle = window.getComputedStyle(p);
  
  let lineHeight = calculateLineHeight( pStyle.getPropertyValue('line-height'), pStyle.getPropertyValue('font-size') )

  //calculate the number of lines in the <p> using the line-height 
  let nbLines = rect.height/lineHeight.value;   //here we have a problem if the unit is something else than px, but no real solution for now

  //determine the gradient colors based on the original text color.
  let colors = isColorDark(pStyle.color) ? COLORS_DARK : COLORS_LIGHT;
    
  //find padding, if not found the default value is 0px
  let paddingTop_parse = CSSDim.parse( pStyle.getPropertyValue('padding-top') );
  let paddingTop = paddingTop_parse.success ?
      						paddingTop_parse.value :
  								new CSSDim(0, "px"); //default value
    
  //before setting the colors, we avoid making all the children transparent by setting a color
  setExplicitColorOnChildren(p.children);
    
  //generate the background value using everything collected so far and set it on the <p>, as well as the other necessary properties
  p.style.background = getGradientBg(colors, lineHeight, nbLines, paddingTop);
  p.style.setProperty("color", "transparent", "important");
  p.style.setProperty("-webkit-background-clip", "text"); //chrome compat
  p.style.backgroundClip = "text";
}

(function() {
  //we style ALL the paragraphs !
  console.time("Time to color paragraphs");
  let paras = document.querySelectorAll("p");
	paras.forEach(lineGradientThisP);
  console.timeEnd("Time to color paragraphs");
})();

